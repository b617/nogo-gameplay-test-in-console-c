﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGoTest
{
    class HumanPlayer : PlayerController
    {
        public override Coordinates MakeMove()
        {
            Console.WriteLine("Player " + getColour().ToString() + ", please make a move:");
            int x, y;
            Coordinates returnValue;
            do
            {
                string[] temp = Console.ReadLine().Trim().Split(' ');
                try
                {
                    x = Convert.ToInt32(temp[0]);
                    y = Convert.ToInt32(temp[1]);
                }
                catch(System.Exception)
                {
                    //if anything goes wrong, try again!
                    x = y = Int32.MaxValue;
                }
                returnValue = new Coordinates(x, y);
            } while (isMoveValid(returnValue) == false);

            return returnValue;
        }
    }
}
