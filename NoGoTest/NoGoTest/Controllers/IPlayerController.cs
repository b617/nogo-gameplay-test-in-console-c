﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGoTest
{
    abstract class PlayerController
    {
        private Game _gameInstance = null;
        private Field colour;

        public PlayerController setGameInstance(Game gameInstance)
        {
            _gameInstance = gameInstance;
            return this;
        }
        public PlayerController setColour(Field colour)
        {
            this.colour = colour;
            return this;
        }

        public Field getColour()
        {
            return colour;
        }
        protected Field[,] getBoardState()
        {
            return _gameInstance.getBoardState();
        }
        protected bool isMoveValid(Coordinates coordinates)
        {
            return _gameInstance.IsMoveValid(coordinates);
        }

        abstract public Coordinates MakeMove();

    }
}
