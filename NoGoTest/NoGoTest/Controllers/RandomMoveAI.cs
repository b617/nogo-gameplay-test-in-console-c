﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGoTest
{
    class RandomMoveAI : PlayerController
    {
        Random rand = new Random();


        public override Coordinates MakeMove()
        {
            Field[,] board = getBoardState();
            int x, y;
            int randMax = board.GetLength(0) * board.GetLength(1);
            Coordinates returnValue;

            do
            {
                int random = rand.Next(randMax);
                y = random / board.GetLength(0);
                x = random % board.GetLength(0);
                returnValue = new Coordinates(x, y);

            } while (isMoveValid(returnValue) == false);


            return returnValue;
        }
    }
}
