﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGoTest
{
    class Game
    {
        private PlayerController player1, player2, currentPlayer;
        Field[,] boardState;

        public Game(PlayerController player1, PlayerController player2)
        {
            boardState = new Field[9, 9];

            this.player1 = player1.setGameInstance(this).setColour(Field.BLACK);
            this.player2 = player2.setGameInstance(this).setColour(Field.WHITE);
            currentPlayer = player1;
        }

        public void Play()
        {
            UpdateGraphics();

            while(!IsGameOver())
            {
                Coordinates move = currentPlayer.MakeMove();
                if (IsMoveValid(move) == false) throw new InvalidOperationException();
                else ApplyMove(ref boardState, move);

                UpdateGraphics();
                NextPlayer();
            }

            //Game finished
            NextPlayer();
            Console.WriteLine("Player " + currentPlayer.getColour().ToString() + " wins!");
        }

        /// <summary>
        /// Do zoptymalizowania
        /// </summary>
        /// <returns></returns>
        bool IsGameOver()
        {
            //if (FindAllValidMoves().Length == 0) return true;

            for (int y = 0; y < boardState.GetLength(1); y++)
            {

                for (int x = 0; x < boardState.GetLength(0); x++)
                {
                    if(boardState[x,y]==Field.EMPTY && IsMoveValid( new Coordinates(x,y) ))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Zoptymalizowac - mogloby brac pod uwage tylko grupy w scislym sasiedztwie punktu
        /// </summary>
        /// <param name="coordinates">Place on the board at which current player tries to place his stone.</param>
        /// <returns>Whether the move can be made.</returns>
        public bool IsMoveValid(Coordinates coordinates)
        {
            //does it fit the board?
            if(!(coordinates.x<boardState.GetLength(0) && coordinates.y<boardState.GetLength(1)))
            {
                return false;
            }
            //is it REALLY trying to put a stone where none is?
            if (boardState[coordinates.x,coordinates.y] != Field.EMPTY) return false;

            //will all groups have at least one liberty after this move?
            Field[,] copyBoard = boardState.Clone() as Field[,];
            ApplyMove(ref copyBoard, coordinates);

            Group[] groups = Group.FindAllGroups(copyBoard);
            foreach (Group group in groups)
            {
                if (group.HasLiberty(copyBoard) == false) return false;
            }

            return true;
        }

        Coordinates[] FindAllValidMoves()
        {
            throw new NotImplementedException();
        }

        void UpdateGraphics()
        {
            Graphics.Graphics.draw(boardState);
        }

        void ApplyMove(ref Field[,] board, Coordinates move)
        {
            board[move.x, move.y] = currentPlayer.getColour();
        }

        void NextPlayer()
        {
            currentPlayer = (currentPlayer == player1) ? player2 : player1;
        }

        public Field[,] getBoardState()
        {
            return boardState.Clone() as Field[,];
        }
    }
}
