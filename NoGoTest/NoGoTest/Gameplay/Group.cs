﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGoTest
{
    class Group
    {
        public List<Coordinates> points = new List<Coordinates>();
        public Field colour;

        /// <summary>
        /// Based on Kosarajus Finding Strongly Connected Components Alghorithm.
        /// Goes through the whole board in linear time, finding all sets of like-colored, orthogonally adjacent stones.
        /// Approximately works in O(2n) time. Where n = size of the board.
        /// </summary>
        /// <param name="board">The board at which said groups will be searched for</param>
        /// <returns>All  black- or white-colored groups. (Does not return groups of empty spaces.)</returns>
        public static Group[] FindAllGroups(Field[,] board)
        {
            bool[,] visited = new bool[board.GetLength(0), board.GetLength(1)]; //visited - which fields you've already gone through
            visited.Initialize();    //fill with FALSE

            List<Group> groups = new List<Group>();

            for (int y = 0; y < visited.GetLength(1); y++) 
            {
                for (int x = 0; x < visited.GetLength(0); x++) 
                {
                    if ((visited[x, y] == false))
                    {
                        Group newGroup = new Group();
                        Stack<Coordinates> toVisit = new Stack<Coordinates>();
                        toVisit.Push(new Coordinates(x, y));
                        visited[x, y] = true;
                        newGroup.colour = board[x, y];

                        while (toVisit.Count > 0) 
                        {
                            //process current coordinate
                            Coordinates current = toVisit.Pop();
                            newGroup.points.Add(current);

                            //add its on-board-neighbours
                            Stack<Coordinates> neighbours = new Stack<Coordinates>();
                            if (current.x > 0) neighbours.Push(new Coordinates(current.x - 1, current.y));
                            if (current.x < board.GetLength(0) - 1) neighbours.Push(new Coordinates(current.x + 1, current.y));
                            if (current.y > 0) neighbours.Push(new Coordinates(current.x, current.y - 1));
                            if (current.y < board.GetLength(1) - 1) neighbours.Push(new Coordinates(current.x, current.y + 1));

                            //validate neighbours - have we checked them already? Are they the same colour as the last one?
                            foreach(Coordinates neighbour in neighbours)
                            {
                                if ((visited[neighbour.x, neighbour.y] == false)
                                    && (board[neighbour.x, neighbour.y] == board[current.x, current.y]))
                                {
                                    visited[neighbour.x, neighbour.y] = true;
                                    toVisit.Push(neighbour);
                                }
                            }
                            neighbours.Clear();
                        }

                        //newGroup is now containing area-group of fields of the same color. We're not interested in colorless,
                        //so we have to get rid of those
                        if (newGroup.colour != Field.EMPTY) groups.Add(newGroup);
                    }
                }
            }

            return groups.ToArray();
        }

        /// <summary>
        /// Finds a group containing a specific point.
        /// Works approximately in O(2n) time, where n = total group size.
        /// </summary>
        /// <param name="board">The board at which the search will commence.</param>
        /// <param name="point">Alghorithm looks for group containing THIS point.</param>
        /// <returns>Group containg given point</returns>
        public static Group FindGroup(Field[,] board, Coordinates point)
        {
            Stack<Coordinates> toVisit = new Stack<Coordinates>();
            Group group = new Group();
            group.colour = board[point.x, point.y];
            bool[,] visited = new bool[board.GetLength(0), board.GetLength(1)];
            visited.Initialize();


            toVisit.Push(point);
            visited[point.x, point.y] = true;

            while(toVisit.Count>0)
            {
                //process current coordinate
                Coordinates current = toVisit.Pop();
                group.points.Add(current);

                //add its on-board-neighbours
                Stack<Coordinates> neighbours = new Stack<Coordinates>();
                if (current.x > 0) neighbours.Push(new Coordinates(current.x - 1, current.y));
                if (current.x < board.GetLength(0) - 1) neighbours.Push(new Coordinates(current.x + 1, current.y));
                if (current.y > 0) neighbours.Push(new Coordinates(current.x, current.y - 1));
                if (current.y < board.GetLength(1) - 1) neighbours.Push(new Coordinates(current.x, current.y + 1));

                //validate neighbours - have we checked them already? Are they the same colour as the last one?
                foreach (Coordinates neighbour in neighbours)
                {
                    if ((visited[neighbour.x, neighbour.y] == false)
                        && (board[neighbour.x, neighbour.y] == board[current.x, current.y]))
                    {
                        visited[neighbour.x, neighbour.y] = true;
                        toVisit.Push(neighbour);
                    }
                }
                neighbours.Clear();

            }

            return group;
        }
        
        /// <summary>
        /// May need some optimisation - some points will be checked 4 times
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        public bool HasLiberty(Field[,] board)
        {
            foreach(Coordinates coords in points)
            {
                if (coords.x > 0)
                {
                    if (board[coords.x - 1, coords.y] == Field.EMPTY) return true;
                }
                if (coords.x < board.GetLength(0) - 1)
                {
                    if (board[coords.x + 1, coords.y] == Field.EMPTY) return true;
                }
                if (coords.y > 0)
                {
                    if (board[coords.x, coords.y - 1] == Field.EMPTY) return true;
                }
                if (coords.y < board.GetLength(1) - 1)
                {
                    if (board[coords.x, coords.y + 1] == Field.EMPTY) return true;
                }
            }
            return false;
        }
    }
}
