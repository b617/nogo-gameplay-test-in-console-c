﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGoTest
{
    enum Field
    {
        EMPTY,
        BLACK,
        WHITE
    }

    struct Coordinates
    {
        public uint x, y;
        public Coordinates(uint x, uint y)
        {
            this.x = x;
            this.y = y;
        }
        public Coordinates(int x, int y)
        {
            this.x = (uint)x;
            this.y = (uint)y;
        }
    }
}
