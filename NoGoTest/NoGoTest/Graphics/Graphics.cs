﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphics
{
    class Graphics
    {
        public static void draw(NoGoTest.Field[,] board)
        {
            int sizeX = board.GetLength(0);
            int sizeY = board.GetLength(1);

            Console.Clear();
            Console.Write('┌');
            drawLine(sizeX);
            Console.WriteLine('┐');
            for (int y = 0; y < sizeY; y++) 
            {
                char[] row = new char[sizeX];
                for (int x = 0; x < sizeX; x++) 
                {
                    row[x] = toChar(board[x, y]);
                }
                Console.Write('│');
                Console.Write(row);
                Console.WriteLine('│');
            }
            Console.Write('└');
            drawLine(sizeX);
            Console.WriteLine('┘');
        }

        static void drawLine(int length)
        {
            for (int i = 0; i < length; i++) Console.Write('─');
        }

        static char toChar(NoGoTest.Field field)
        {
            switch(field)
            {
                case NoGoTest.Field.EMPTY:
                    return ' ';
                case NoGoTest.Field.WHITE:
                    return 'O';
                case NoGoTest.Field.BLACK:
                    return 'X';
                default:
                    throw new Exception("Invalid NoGoTest.Field value! (in Graphics.toChar()");
            }
        }
    }
}
