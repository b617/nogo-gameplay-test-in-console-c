﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGoTest
{
    class Menu
    {
        const string art = " ____    ___    ____   ___  \n|    \\  /   \\  /    | /   \\ \n|  _  ||     ||   __||     |\n|  |  ||  O  ||  |  ||  O  |\n|  |  ||     ||  |_ ||     |\n|  |  ||     ||     ||     |\n|__|__| \\___/ |___,_| \\___/ \n                            \n";

        public Menu()
        {
            Console.WriteLine(art);
            Console.WriteLine("Welcome to NoGo game test. Press any key to start.");
            Console.ReadKey();
        }

        public Game CreateGame()
        {
            Console.Clear();
            PlayerController player1 = PlayerSelection("Please select controlling type for player 1.");
            Console.Clear();
            PlayerController player2 = PlayerSelection("Please select controlling type for player 2.");

            return new Game(player1, player2);
        }


        PlayerController PlayerSelection(string message)
        {
            int read = 0;
            while(true)
            {
                Console.WriteLine(art);
                Console.WriteLine(message);
                Console.WriteLine("1. Controlled by a human.");
                Console.WriteLine("2. Controlled by AI making random moves.");

                try
                {
                    read = Convert.ToInt32(Console.ReadLine());
                }
                catch(Exception)
                {
                }

                if (read == 1) return new HumanPlayer();
                if (read == 2) return new RandomMoveAI();

                Console.Clear();
                Console.WriteLine("Incorrect input. Try again.");
            }
        }
    }
}
