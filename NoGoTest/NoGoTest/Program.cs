﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoGoTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu mainMenu = new Menu();
            Game game = mainMenu.CreateGame();
            game.Play();

            Console.WriteLine("\n\n\nThank you for playing!\nTo quit application press any key.");
            Console.ReadKey();
        }
    }
}
